[Akademy](https://akademy.kde.org/2021) 2020 was hosted online from the 4th to the 11th of September, 2020. The annual conference of the KDE Community this year featured training sessions on its first day, a two-day conference during the weekend, with presentations on the latest KDE developments, plus an extra day of talks on the following Friday. During the week, there were five days of workshops, Birds of a Feather (BoF) sessions and coding sessions. Akademy this year welcomed more than 60 speakers, attracted 225 attendees from 79 countries and reached the global KDE Community and beyond.

<div style='text-align: center'> <figure style="padding: 1ex; margin: 1ex;"><a href="images/Akademy/group.png"><img src="images/Akademy/group.png" width="100%" /></a><br /><figcaption>Akademy group photo.</figcaption></figure> </div>

### Day 1 - Training Sessions

Akademy kicked off with training sessions on several KDE-related topics.

Nuno Pinheiro from [KDAB](https://www.kdab.com/) conducted a class on UI/UX Design in QML for Desktop. This online workshop contained practical exercises on dos, don'ts, and integration; and tips on the outside of the box UI design. The session started out with attendees relaying to Nuno the projects they were working on and how they hoped the lesson would help them.

Meanwhile, in room 2, Milian Wolff, another Senior Software Engineer at KDAB, taught us about Debugging and Profiling on Linux. This training was at a slightly higher level and required some knowledge and experience with Qt and C++ as well as a basic understanding of multithreaded programming. The session covered the most essential debugging and profiling tools on Linux and attendees learned how to use the tools and how to interpret the results.

In room 3, Michael Friedrich, a Developer Evangelist at [GitLab](https://about.gitlab.com/), told us how to Speed Up Your Development Workflows with GitLab in his best practice workshop. Michael took us through our first steps in GitLab with project management (issues, boards, labels, templates, etc.) and combined it with development workflows. We learned how to start our first merge request to solve an issue and got ideas on branching, code reviews, approval processes and added automated CI test feedback in MRs.

Albert Astals Cid conducted a session called Introduction to QML in room 1. Albert taught us how to compose fluid user interfaces using the QML language and we also learned how to hook the QML side up to the business logic in C++. In room 2, David Faure told us about Multithreading in Qt, which is essential for developers who want to create fast and responsive applications on computers, phones, and embedded devices with an increasing number of cores.

Finally, for something quite different, Dr. Carlee Hawkins talked about Implicit Bias in room 3. In this session, we were asked to consider our own biases regarding race, gender, age, etc. We explored how researchers understand biases and learned how to mitigate the influence of our own biases on our thoughts.

The advice offered by Dr. Hawkins will help us make KDE more welcoming and diverse for everybody.

### Day 2 - Conference

The first day of Akademy talks were varied and interesting, covering a wide range of topics, from managing project goals and technical advances in Qt and KDE technologies, to Open Source in dentistry and Linux in automobiles.

Aleix Pol, President of KDE, kicked off the day at 8:50 UTC sharp by playing a [video](https://youtu.be/v07r6_6E8TQ) made by Bhavisha Dhruve and Skye Fentras welcoming everybody to the event. After acknowledging the very special circumstances of this year's Akademy, Aleix introduced the first [keynote speaker Gina Häußge](https://youtu.be/8NJzb9I0PXY).

Gina is the creator and maintainer of OctoPrint, a highly successful and feature-rich system for controlling your 3D printer over a web interface. Gina used her time to reveal the good and not so good things about becoming an independent Open Source maintainer. She talked about the sense of freedom and purpose gained through Open Source work, but also the downsides of monetary instability and frequently feeling on her own despite working for hundreds, maybe thousands of users. Despite these disadvantages, she happily admitted that she would do it all over again, that the sensation of helping others and the fulfillment she experienced made up for all the darker patches.

After that it was time for another veteran Open Source contributor: Jonathan Riddell talked about his steering of one of KDE's current Community-wide goals: It's [All about the Apps](https://youtu.be/mlU97vqIkgk), the project in which KDE community members work to promote and distribute KDE applications on their own merits, beyond their link to KDE's Plasma desktop. Jonathan gave us the motivations behind proposing the goal and its evolution since it was officially announced in Akademy 2019. Likewise, Niccolo Venerandi talked about the [Consistency](https://youtu.be/f_74zq8IcOw) goal. This goal seeks to unify the look and feel of Plasma and all KDE apps to provide a coherent experience to users. Niccolo pointed out that Plasma does not have serious consistency problems, but different approaches to design in apps that sometimes lead to a bewildering array of looks and behaviors. Niccolo then showed us the future of KDE applications and, frankly, it looks amazing.

The presentations covering individual goals wound up with Méven Car talking about [Wayland](https://youtu.be/GFPAwA5xVEc). It is no secret that the ride of porting KDE software and technologies to Wayland, the replacement for our venerable X window system, is being a bumpy one. That is why the KDE Community decided to make Wayland a priority. The Wayland goal is a big task requiring updates to multiple components and forcing to refactor KDE's whole display stack. But as Méven explained, the community has made significant progress since Akademy 2019.

Following the presentation of individual KDE goals, Niccolo Venerandi, Méven Car, Jonathan Riddell, Lydia Pintscher and Adam Szopa got together for a round table that tackled how the first year of their goals went and what they learned along the way.

Following the round table, Andreas Cord-Landwehr used a ten-minute fast track slot to talk about [SPDX](https://youtu.be/dDNj15o9YRY), a system for better license statements. In the talk, we learned that SPDX identifiers are an important step towards enabling automatic tooling for checking license statements. Andreas explained the advantages of using license statements and how simple it is to apply them. He also gave a short overview of what has already happened inside the KDE Frameworks and where contributors could help to support the conversion to SPDX.

Then Shawn Rutledge covered [Editing Markdown with QTextDocument](https://youtu.be/zDfxM6vcuEo) in another 10-minute talk. Shawn added markdown support in Qt 5.14 as a first-class format and as an alternative to the limited subset of HTML that QTextDocument had traditionally used. During the talk, he demoed WYSIWYG editors written with widgets and with Qt Quick.

In the final fast track talk before lunch, Carl Schwan expounded on [How to Create a Good Promotional Website for your Project](https://youtu.be/k8jSULzq-tQ). Carl has been the main developer behind the overhaul of many of KDE's main sites, including kde.org. During the talk, Carl presented the KDE Jekyll theme, the motivation behind the project, and briefly explained how it could be used to create a KDE website. He also showed some counter-examples, examples of poorly designed websites and how they could be improved to make the projects more attractive to potential users.

Later, things started with a presentation from the KDE e.V. Board and reports from the Working Groups. The Board told the attendees all about the things they had done over the year since the last Akademy. Highlights included expanding the number of paid employees from three to five, the migration to GitLab, and the funding of more support for community members. The Board followed up with details on the activities of the different working groups, although some of their presentations had to be moved to the end of the day due to time constraints.

Then we launched back into the talks’ proper with the [Input Handling Update](https://youtu.be/uKx6x0qlvnY), again by Shawn Rutledge. In this talk, Shawn talked about what's coming up and several goals for input events in Qt 6.

Meanwhile, in Room 2 Cornelius Schumacher was talking about [the KDE Free Qt Foundation](https://youtu.be/rn7jfrJWuAM). Established in 1998, the KDE Free Qt Foundation was founded to keep the Qt toolkit free for KDE and all other free software projects. The Foundation has held steady during the more than two decades of sometimes turbulent times Qt and KDE have gone through together. Cornelius told the story of how this worked.

And speaking of The Qt Company... A little later, back in Room 1, Richard Moe Gustavsen, talked about [Native Desktop Styling Support for Qt Quick Controls 2](https://youtu.be/39w-CjCxcVk) and the ongoing work at The Qt Company to support writing desktop applications using Qt Quick Controls 2.

At the same time, in Room 2, Aleix Pol was talking about [KDE's Products](https://youtu.be/gj4XV0PMKZw) and how to visualise their relationship with users. In the talk, Aleix introduced a framework to help developers make sure the Free Software community and its users are best taken care of.

In the next slot, Patrick Pereira presented in Room 1 [QML Rapid Prototyping -- Developing tools to improve QML prototypes and development](https://youtu.be/GpNP7eJCvHs). In his talk, Patrick talked about how QML prototyping is something that all developers do, and how it can be achieved more efficiently. He used two projects as examples: QHot (a hot reload for nested QML files) and QML Online (an online QML editor created with WebAssembly) to help explain how to bring down the development time and learning curve for QML.

In Room 2, Johan Thelin introduced his talk [Linux in Cars - So What?](https://youtu.be/ik6632xh6ZQ) from, get this, inside his car. Literally. Johan talked about why cars are still also using so much proprietary software you would be hard pushed to find the Open Source bits, even though they may be using Linux deep down. He also talked about what needed to be addressed to improve the situation and how KDE software could work for those use cases.

<div style='text-align: center'> <figure style="padding: 1ex; margin: 1ex;"><a href="images/Akademy/Day2_Jonah_Thelin_Linux_cars.jpg"><img src="images/Akademy/Day2_Jonah_Thelin_Linux_cars.jpg" width="100%" /></a><br /><figcaption>Jonah Thelin talks Linux in cars from a car.</figcaption></figure> </div>

Following this batch of regular talks, there were another three 10-minute fast track presentations.

In Flatpak, [Flathub and KDE: A Quick Summary](https://youtu.be/xW9uTxAoULU), Albert Astals Cid introduced the audience to Flatpak, explained what Flathub was and how KDE interacted with both of them.

Then Nicolás Alvarez spoke of [Improving KDE Server Infrastructure](https://youtu.be/NXd11XMf-Vo), the formation of the Sysadmin Working Group, and told attendees how the Sysadmin team was making KDE servers more manageable by reducing "technical debt", moving manual tasks into scripts, improving documentation, and making more things testable locally before putting them on the real servers.

In the last fast track of the day, David Edmundson gave tips on [How to Win an Argument with a Maintainer](https://youtu.be/-HFU3j2pbWs), having partaken in and witnessed hundreds of discussions on Bugzilla and Phabricator that then turned into arguments that yielded angry stalemates. He shared with the audience the methods he had seen work to achieve happy mediums and warned against attitudes that escalated situations into miserable experiences for everybody.

Next came one of the more surprising presentations of the day, delivered by Tej Shah, a Doctor of Medicine in Dentistry from the US. Tej talked about his project [Clear.Dental](https://youtu.be/RgPl4JoApxY) and his attempt to move dentistry to Open Source using the power of Linux, Qt, and KDE. He reviewed the state of Dental software (which is pretty dire), the problem with the current software available, and how Clear.Dental could contribute to solving it.

At the same time, in Room 2, Camilo Higuita was talking about his own passion project: Maui and gave the audience a rundown of all the updates on the group of apps, services, libraries, and UI (User Interface) frameworks Maui provides to produce attractive-looking applications.

In the next session, Rohan Garg gave attendees a lesson in [Linux Graphics 101](https://youtu.be/UvbhIre5no0) in which he explained how the growing popularity of ARM devices has led to platform architectures with quirkier graphics hardware. He talked about the basics of how the Linux graphics stack works and the history behind how we've come to the current Gallium design in Mesa.

Finally, [Google Summer of Code](https://summerofcode.withgoogle.com/) participant Amy Spark showcased how she [Integrated Hollywood Open Source with KDE Applications](https://youtu.be/_m2Zo6gDP6g) by porting a Disney Animation technology to Krita. SeExpr gives Krita artists access to procedurally generated texturing, allowing for fine surface details, lighting effects, overlays, and more to be added at the push of a button. As a scripting language, it gives creators the flexibility needed to ensure perfect results every time by tailoring the algorithm to their needs. Amy had to overcome many technical barriers during the porting process, as SeExpr was originally built to run only on a very specific proprietary stack.

<div style='text-align: center'> <figure style="padding: 1ex; margin: 1ex;"><a href="images/Akademy/Day2_David_Revoy_SeExpr.jpg"><img src="images/Akademy/Day2_David_Revoy_SeExpr.jpg" width="100%" /></a><br /><figcaption>Example from David Revoy on how SeExpr can be used to create art.</figcaption></figure> </div>

### Day 3 - Conference

Day 2 of the conference stretch of Akademy (day 3 of the overall event) kicked off with a heavy-duty programming courtesy of Ivan Čukić who talked about [C++17 and 20 Goodies](https://youtu.be/-ev7uvKh_JY). Most KDE applications are developed using C++ so Ivan covered the new features that C++17 and 20 bring and how they could be combined with each other.

Something more user-centric was going on in Room 2, where Marco Martin and Aditya Mehra were [Showcasing Plasma Bigscreen](https://youtu.be/vMVd3q3bfx4), KDE's interface for large smart TVs. Marco and Aditya took attendees through the various features and the technology that powers Bigscreen, such as Plasma, Kirigami, and KDE Frameworks with a touch of Mycroft's open-source voice assistance platform. You can try Bigscreen now by burning it to a micro SD card and loading it into a Raspberry Pi 4 hooked up to your TV.

<div style='text-align: center'> <figure style="padding: 1ex; margin: 1ex;"><a href="images/Akademy/Day3_PlasmaBigscreen.jpg"><img src="images/Akademy/Day3_PlasmaBigscreen.jpg" width="100%" /></a><br /><figcaption>Plasma Big Screen - Plasma for your TV.</figcaption></figure> </div>

In the next slot, the audience in Room 1 was subject to another talk about programming languages, in this case, Rust from a KDE Perspective. In this talk, Méven Car explained what Rust could offer to the KDE developer community and the features that made it a unique programming language.

In Room 2, we learned about a success story of KDE in the Real World™: back in October 2019, the staff of Janayugom, a local daily newspaper in Kerala with 100,000 readers, decided to move their publication to Free Software. In his presentation on [Free Software, Press Freedom & KDE](https://youtu.be/XoAJ0Wl3z9U), Ambady Anand S., a sysadmin involved in the move, told us the story of how the migration went. Later, back in Room 1, Andreas Cord-Landwehr introduced a different way of developing in his talk [Test It!](https://youtu.be/lwJoqgqvFx0) – Unit testing for lazy developers. Andreas proposed developers turn things on its head and prepare tests for the code before actually writing the code. He argued that code written to pass tests was leaner and more focused. Andreas also discussed why automated tests are important for projects and strategies on how to design them.

Meanwhile, in Room 2, Timothée Giet was [Celebrating 20 Years of GCompris](https://youtu.be/IBJrjt16mGk), as well as the fact that the universally acclaimed classic FLOSS toolset for teachers is nearing version 1.0. It only took two decades! Seriously though, Timothée showed us some of the new activities which are coming to GCompris, such as new counting and arithmetic games, and a fun-looking electric circuits simulator.

Next up in Room 1, David Faure told us about [KIO: A Story of Young and Old Jobs](https://youtu.be/bOUV8KQGP8I). The presentation aimed at application developers and KIO contributors gave an overview of the job mechanism as it is used in KIO, laid out the jobs added in the last two months and explained the concept of "delegates" which are used to solve the inverse dependency problem. In Room 2, Marta Rybczynska told us about her [Year in KDE from Outside](https://youtu.be/U6L3TYXPVHc). During her year "away," she analyzed media reporting on KDE and tracked what news sites, blogs, and podcasts chose to focus on when they talked about KDE.

Next up in Room 1 Daniel Vrátil talked about [Static Code Analysis with Gitlab CI](https://youtu.be/SkMG4gwNKVI). Daniel showed the benefits of using static analysis tools and linters to automatically check code quality, and explained how to configure GitLab CI to run those tools automatically on each pull request or as a part of regular builds.

In Room 2, Leinir and Andrew Shoben literally couldn't hide their excitement while presenting [KDE Wags Your Tail](https://youtu.be/VeNJAXCFPXo), in which they explained how to control animatronic tails and ears using software based on free software and KDE's Kirigami framework.

The first set of the day's talks wound up with three 10-minute fast track talks in which Bhushan Shah talked about his [experience with and gave advice on online sprints](https://youtu.be/jf2Miy5JQAY); Adriaan de Groot explained the [Fiduciary License Agreement](https://youtu.be/eIqTsT6UtT0), a tool that the KDE community uses to manage licensing in the long-term; and Kai Uwe Broulik revealed some of the less obvious tips and tricks to get the most out of KDE's Plasma desktop.

After a four-hour recess, the KDE Community got together again to listen to the event sponsors. KDE displayed deep gratitude to Canonical, KDAB, MBition, openSUSE, GitLab, Froglogic, Collabora, CodeThink, FELGO, DorotaC.eu, The Qt Company, Pine64 and Tuxedo for their generosity thanks to which Akademy is made possible.

Next up in Room 1, Dimitris Kardarakos talked about [Creating a Convergent Application](https://youtu.be/P7WmvuLoZLw) Following the KDE Human Interface Guidelines. Dimitris introduced attendees to the primary components of Kirigami and showed how an application can look equally good on the desktop and mobile. Using the Calindori calendar app as an example, Dimitris aimed to inspire attendees to create their own Kirigami applications.

Meanwhile, in Room 2, Nicolas Fella explained in [Konquering the Droids](https://youtu.be/7F99hZBiF0c) that, to stay relevant, KDE needed to expand from its traditional desktop space and into the mobile world. He argued the most realistic and quickest way to do that was to create apps for the Android ecosystem, and explained the porting process.

Aleix Pol took to Room 1's virtual stage again in [Getting into KWin and Wayland](https://youtu.be/xVz3VgNjexY) to explain how contributors could get involved with the development of KDE's Wayland experience.

In Room 2, Doctor Luis Falcón told us about [MyGNUHealth: GNU Health Goes Mobile with KDE and Kirigami](https://youtu.be/5JvLuHDKZzU). GNU Health (GH) is a Libre Health and Hospital Information System which has been deployed in many countries around the globe, from small clinics to very large, national public health implementations. MyGNUHealth is the GH's Personal Health Record application that integrates with the GNU Health Federation and is focused on mobile devices. Dr. Falcón told us about what led him to choose KDE's Kirigami framework to develop MyGNUHealth and the technical insights gained by the community behind the project.

<div style='text-align: center'> <figure style="padding: 1ex; margin: 1ex;"><a href="images/Akademy/Day3_LuisFalconMyGNUHealth.png"><img src="images/Akademy/Day3_LuisFalconMyGNUHealth.png" width="100%" /></a><br /><figcaption>Luis Falcón M.D. presents MyGNUHealth, a new KDE project.</figcaption></figure> </div>

Following these two talks, Neal Gompa presented [Fedora KDE](https://youtu.be/5Laat0XoWZE) in Room 1, explained how it started and what makes it special within Fedora; while at the same time in Room 2, Aniqa Khokhar introduced us to [Change Management](https://youtu.be/rs0HT-cjD34) and helped us learn how to accept changes and newcomers in KDE.

A bit later in Room 1, Volker Krause showed how, by [Using Wikidata and OpenStreetMap data](https://youtu.be/WS1XUW_4FcY), it was possible to make applications smarter. Volker went into depth and explained how those two data sets are structured, how they can be accessed, how to comply with their licenses, and how developers can make use of them for their apps.

In Room 2 Catharina Maracke spoke to attendees about [Open Source Compliance](https://youtu.be/XrDfZaoesj0). Catharina told us about how in today's complex world of OSS license compliance, it is very important to know the basics of copyright and licensing structures as well as some of the relevant tips and tricks for the most common OSS licenses.

A little later in Room 1, David Edmundson and Henri Chain co-hosted a talk on [Next Generation Application Management](https://youtu.be/LVkxmHn_3bQ) and explained how using cgroups, developers could contribute to making everything amazing.

And, speaking of amazing, in Room 2, Massimo Stella told us about [Kdenlive's Journey to Being a Leading Open Source Video Editor](https://youtu.be/bayOuCvzOio). He showed us a video of Kdenlive's new capabilities and provided a live demo of the new features of KDE's powerful video editing software.

The regular talks wrapped up with two technical talks aimed at developers: In Room 1 Carson Black talked about [API Design and QML](https://youtu.be/KqxZs7aOFMw) and in Room 2 Benjamin Port explained [how Plasma's System Setting have recently all been ported to KConfigXT and the advantages gained from the move in System Settings: Behind the Scenes](https://youtu.be/QeOHV8myQ5E).

To finish off the day, we had a star keynote presentation delivered by Nuritzi Sanchez, Senior Open Source Program Manager at GitLab and prior President and Chairperson of the GNOME Foundation. In her presentation, [Open Source Resilience and Growth: Creating Communities that Thrive](https://youtu.be/bhn5ZKmW6Jg), Nuritizi talked about some of the initiatives that KDE is involved in that help it becomes a more resilient community, while at the same time pointing out areas of opportunity. She also explored topics around building more diverse and inclusive communities, including collaborative communication, and ideas for outreach.

From Monday to Thursday, Akademy attendees participated in Bird of a Feather (BoF) meetings, private reunions, and hackathons.

### Day 7 - The Last Day

It was a fine day in Onlineland and Akademy attendees were in a festive mood, not least because they were ready to celebrate the successful migration of KDE to GitLab. Although a titanic effort, the move is already paying off, as GitLab offers an easier and more flexible platform for developers and users to get their work done and shared.

Ben Cooksley, sysadmin extraordinaire, Bhushan Shah, Plasma Mobile's main developer, Community veterans like David Edmundson and Lydia Pintscher, and many others shared their experiences of how the migration has improved the way they worked.

GitLab was also represented in the party with Nuritzi Sanchez, Senior Open Source Program Manager at GitLab, attending.

### BoFs

Then there were several interesting BoFs throughout the day covering, in typical KDE fashion, a very wide range of topics.

Aniqa Khokhar and Allyson Alexandrou hosted a meeting on The KDE Network, KDE's initiative to start and support grassroots organizations in different parts of the world. Cornelius Schumacher told us about Blue Angel, an official label from the German government that is awarded to eco-friendly products. As KDE has already proven to have a low carbon footprint and helps recycle old machines, Cornelius thinks the Community should work harder to become even greener and get recognized for our efforts. Carl Schwan managed a meeting on what KDE should do to improve the online documentation for developers, and David Edmundson met with community members interested in pushing the development of Qt Wayland forward.

### Talks

Later in the day, we attended the first batch of student presentations. Mentoring students is an essential part of KDE's mission, as they can often receive through events such as Google Summer of Code and Season of KDE, valuable experience and get started in contributing to Free Software.

First up was Kartik Ramesh who worked on facial recognition in [digiKam](https://www.digikam.org/). If you have been following the latest releases of digiKam, KDE's professional photograph management software, you will be aware of how to face recognition has changed and improved over the last few versions. Kartik worked on the front end interface to make it friendly and usable. Deepak Kumar, on the other hand, worked on multiple datasets for [GCompris](https://gcompris.net/index-en.html), the activity-packed educational software for children. He added a tutorial screen to the Odd & Even game and also new datasets to (read "made more exercises for") the Clock game, Balance Scales, and more.

Sharaf Zaman SVG worked on mesh gradients for [Krita](https://krita.org/en/), KDE's application for painters, thus improving Krita's support for SVG images; and Sashmita Raghav improved the timeline clip color palette for Kdenlive, KDE's video editor.

It was then time for Kevin Ottens to warn us about Lost Knowledge in KDE. He explained how it is possible to lose knowledge over time because people leave the Community, advances in technology go undocumented and then forgotten, and software gets deleted. Kevin finished his talk by speaking of ways to avoid losing this information in organizations like KDE.

Kevin's talk was followed by another batch of student presentations in which Sashwat Jolly talked about incorporating an EteSync agent into [Akonadi](https://userbase.kde.org/Akonadi), [Kontact's](https://kontact.kde.org/) backend for storage indexing and retrieval of users' personal information. EteSync is a free software service you can self-host and that provides an end-to-end encrypted, and privacy-respecting sync for contacts, calendars and tasks.

Then Shivam Balikondwar spoke of how he added file backends for the ROCS IDE and how he added KML files to the list of types of files ROCS could parse. Meanwhile, Paritosh Sharma worked on bringing 3D to KStars by incorporating Qt3D into the stargazing app.

Finally, Anaj Bansal explained how he worked on improving KDE's web infrastructure and helped port kde.org to Hugo.

After this batch of students' presentations, it was time for another conference talk, and Nate Graham told us about his [Visions of the Future](https://youtu.be/qY510rNMW0M). Nate, among other things, wanted us to envision a world in which KDE Plasma gets shipped by default on every PC, phone, and tablet on the planet (and possibly off it too). It is worth pointing out that Nate had already presented a talk called "Konquering the world -- A 7 step plan to KDE world domination" at Akademy 2018. We may be detecting a trend here...

After a glimpse into tomorrow, quite appropriately the next generation of KDE contributors took again to the stage and shared their work. There was a theme here too, because Saurabh Kumar told us how he implemented a storyboard editor as a docker for [Krita](https://krita.org/en/); L.E. Segovia spoke of their work with dynamic fill layers using SeExpr, again for Krita; and Ashwin Dhakaita told us how he had managed to integrate MyPaint brushes... into Krita.

The only discordant note came from Kitae Kim, who spoke of how he improved MAVLink integration in Kirogi, KDE's ground control software for drones.

The final presentation of the day and the very last of Akademy 2020 was delivered by KDE veteran Valorie Zimmerman. Valorie told us [how to avoid burnout](https://youtu.be/OTAh1_YXXb0) and advised us on how to recognize the signs, gave practical advice on what steps we could take to not let it affect us and then opened the floor to questions and stories from other Community members. An appropriately heart-warming, feel-good final talk.

### Akademy awards

Then it was the moment to celebrate individual achievements with the traditional [Akademy Awards](https://youtu.be/Ikxly74nECE).

Presented by last year's winners, Volker Krause, Nate Graham and Marco Martin, the award to Best Application went to Bhushan Shah for creating a new platform, [Plasma Mobile](https://www.plasma-mobile.org/), on which new applications could thrive. The prize to Best Non-Application was given to Carl Schwan for his work of revamping KDE's websites; and the special Jury Award went to Luigi Toscano for his work on localization.

Finally, the jury awarded a special Organization Prize to the Akademy Team made up of Kenny Coyle, Kenny Duffus, Allyson Alexandrou and Bhavisha Dhruve, for their work organizing such a very special event.

Aleix Pol, President of KDE e.V., delivered the final words of the event and pronounced closed what has been an amazing edition of Akademy in so many different ways.

### Acknowledgements

Despite the special circumstances, Akademy 2020 was a resounding success. The conference part had speakers and audience connecting from all over the world, and attendance was up by 36% with regard to last year.

There were no major problems, no talk or event had to be postponed or cancelled due to technical issues; everybody was able to connect, watch, listen and participate in the activities without having to install special software; and organizers were able to see the result of months of hard work come to fruition.

All of this deserves a round of applause for, first, the Akademy team, who, in what seemed like adverse circumstances, were not only nimble enough to pivot and avoid cancelling the event, but set up an exciting alternative with fascinating content. Second, the creators of BigBlueButton, a sturdy and reliable FLOSS videoconferencing system that allowed KDE to move Akademy online with the confidence that things would work as they should.

And, finally, KDE's Sysadmin team, who set up all the technical backend and were on guard 24/7 during the whole event, adjusting bandwidth and tweaking settings, to ensure everybody could enjoy a glitch-free Akademy.