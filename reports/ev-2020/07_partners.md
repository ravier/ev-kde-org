### [Advisory Board](https://ev.kde.org/advisoryboard.html)

The [KDE Advisory Board](https://ev.kde.org/advisoryboard.php) is a group of representatives of KDE e.V.'s patrons and other select organizations that are close to KDE's mission and community. It currently has 13 members, and there is a KDE e.V. working group with community members acting as direct contacts for our partners.

In 2020, some of the Advisory Board members had a chance to meet in person with the KDE e.V. board in February at FOSDEM, to talk over various topics of mutual concern.

Additionally, the Advisory Board held a call in July 2020, with the goal always being to inform the members of the Board, receive feedback and discuss topics of common interest.

One of the main topics of discussion during the call revolved around the status of Qt and the role of the KDE Free Qt Foundation. We also discussed our 2nd trinity of KDE Goals and the changes that took place in the e.V. since the previous call, including the new members of the Board of Director's and our new contractors.

In addition, the members were briefed on the status of our major events (Akademy, LAS) and all the development sprints that were being planned to take place online for the first time due to the measures against COVID-19 across the world. We discussed various challenges that the members also faced during the pandemic and exchanged ideas on tackling them.

The Advisory Board is a place and a symbol of KDE’s collaboration with other organizations and communities firmly standing behind the ideals of Free and Open Source Software.

Its current members include Blue Systems, Canonical, City of Munich, Debian, enioka Haute Couture, FOSS Nigeria, FSF, FSFE, OpenUK, OSI, SUSE, The Document Foundation, The Qt Company.

### [Patrons](https://ev.kde.org/supporting-members.html)

**Current patrons:** Blue Systems, Canonical, enioka Haute Couture, Google, The Qt Company, Slimbook, SUSE.

**Current supporters:** KDAB, basysKom, Kontent.

### [Community Partners](https://ev.kde.org/community-partners.html)

**Current community partners:** Qt Project, Lyx and Randa Meetings.