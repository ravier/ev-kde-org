---
title: 'GNOME Foundation and KDE e.V. to Co-Host Conferences in 2011'
date: 2010-03-16 00:00:00 
layout: post
---

Following the successful Gran Canaria
Desktop Summit in 2009, the GNOME Foundation and KDE e.V. Boards have
decided to co-locate their flagship conferences once again in 2011,
and are taking bids to host the combined event. The Desktop Summit
2011 will be the largest free desktop event ever.

The <a href="http://dot.kde.org/2010/03/16/desktop-summit-2011-call-hosts">call for hosts</a> is available.
      
<p>In July of 2009, the GNOME and KDE communities came together for the
Gran Canaria Desktop Summit, the first co-located KDE/GNOME event. It
was a major success, and was a fantastic opportunity for the leaders
of the free software desktop efforts to share talks, communicate on
common issues, and attend combined social events. The attendees from
both projects expressed great interest in repeating the event and
merging the programmes to synchronize schedules and make the event an
even greater opportunity for the KDE and GNOME teams to learn from
each other and work together.</p>

<p>"The Gran Canaria Desktop Summit was a great first event," said
Vincent Untz, GNOME Foundation Board Member. "We enjoyed working with
our KDE friends at GCDS in 2009, and want to increase our cooperation
in 2011. We plan to go beyond simple co-location this time, and
actually plan a combined schedule in 2011 so that KDE and GNOME
contributors have every opportunity to work with and learn from each
other."</p>

<p>The combined summit is also an opportunity for commercial sponsors of
the GNOME and KDE projects to meet with the contributors from KDE and
GNOME and to help foster faster collaboration and development of the
free software desktop. Sponsors of the first Desktop Summit have
expressed great interest in seeing both communities working together
again.</p>

<p>The GNOME and KDE projects will hold independent events in 2010.
GUADEC, the GNOME Project's annual conference, will be held in The
Hague, Netherlands on July 24 through July 30 of this year. KDE's
Akademy will be located in Tampere, Finland from July 3 to 10 this
year. Both groups will likely hold smaller sprints through 2010 and
early 2011 to prepare for the combined 2011 Desktop Summit.</p>

<p>"The KDE e.V. board felt that GCDS was a fantastic event, and we
learned what works well and what can be improved when co-hosting an
event with our GNOME friends," said Cornelius Schumacher of the KDE
e.V. "KDE and GNOME share a lot of goals for the free desktop, as well
as technology, so we're excited to make use of this experience and
have an opportunity to co-locate again in 2011."</p>

<p>More than 850 contributors to the GNOME and KDE projects gathered in
Gran Canaria last July. The event brought together attendees from 50
countries, and helped raise local awareness of free software and had a
measurable impact on the local community. The impact of the event
continues to be felt even after the event, with nearly 2 million hits
to the summit Web site following the event.</p>

<p>"We were thrilled to have GCDS right here, and felt that it was an
enormous boost for our local commitment to free software," said José
Miguel Santos Espino, Director of IT at the University of Las Palmas
de Gran Canaria. "It's hard to overstate how important it was to have
the opportunity to meet with contributors from GNOME and KDE and learn
more about what's possible on the desktop with free software."</p>

<p>The projects are seeking a host in Europe at a location that can
handle more than 1,000 participants. For detailed requirements,
prospective hosts can see the requirements for Akademy
(http://ev.kde.org/akademy/requirements.php) and GUADEC
(http://live.gnome.org/GuadecPlanningHowTo/CheckList). Applications
are welcomed before May 15th and should be sent to the KDE e.V.
(kde-ev-board@kde.ev) and the GNOME Foundation (board-list@gnome.org)
boards.</p>

<p>
For further information, or for media enquiries, please contact
<a href="mailto:board@gnome.org">board@gnome.org</a> and <a
href="mailto:kde-ev-board@kde.org">kde-ev-board@kde.org</a>.
</p>

<h2>About KDE and the KDE e.V.</h2>

<p>
KDE is an international technology team that creates free and open source
software for desktop and portable computing. Among KDE's products are a modern
desktop system for Linux and UNIX platforms, comprehensive office productivity
and groupware suites and hundreds of software titles in many categories
including Internet and web applications, multimedia, entertainment, educational,
graphics and software development. KDE software is translated into more than 60
languages and is built with ease of use and modern accessibility principles in
mind. KDE4's full-featured applications run natively on Linux, BSD, Solaris,
Windows and Mac OS X.
</p>

<p>
KDE e.V. is the organization that supports the growth of the KDE community. Its
mission statement -- to promote and distribute Free Desktop software -- is
provided through legal, financial and organizational support for the KDE
community. KDE e.V. organises the yearly KDE World Summit "Akademy",
along with numerous smaller-scale development meetings.
</p>

<p>
More information about KDE and the KDE e.V. can be found at <a
href="http://www.kde.org">www.kde.org</a> and <a
href="http://ev.kde.org">ev.kde.org</a>.
</p>

<h2>About GNOME and the GNOME Foundation</h2>

<p>
GNOME is a free-software project whose goal is to develop a complete, accessible
and easy to use desktop for Linux and Unix-based operating systems. GNOME also
includes a complete development environment to create new applications. It is
released twice a year on a regular schedule.
</p>

<p>
The GNOME desktop is used by millions of people around the world. GNOME is a
standard part of all leading GNU/Linux and Unix distributions, and is popular
with both large existing corporate deployments and millions of small business
and home users worldwide.
</p>

<p>
Comprised of hundreds of volunteer developers and industry-leading companies,
the GNOME Foundation is an organization committed to supporting the advancement
of GNOME. The Foundation is a member directed, non-profit organization that
provides financial, organizational and legal support to the GNOME project and
helps determine its vision and roadmap.
</p>

<p>
More information about GNOME and the GNOME Foundation can be found at
<a href="http://www.gnome.org">www.gnome.org</a> and <a
href="http://foundation.gnome.org">foundation.gnome.org</a>.
</p>
