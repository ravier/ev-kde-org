---
title: 'Akademy 2020 Board Election Results'
date: 2020-09-07 15:42:04 
layout: post
---


During the Annual General Meeting of KDE e.V. at Akademy 2020
elections were held for three board positions. The terms
for Aleix, Eike and Lydia were expiring. All three ran for
re-election and were re-elected by the membership of the association.
As a result, the [board and the board-roles](https://ev.kde.org/corporate/board/) are unchanged
relative to 2019. The newly-re-elected members will serve 3 year terms.
