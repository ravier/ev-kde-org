---
title: 'New KDE e.V. Statutes'
date: 2004-10-01 00:00:00 
layout: post
---

Effective from 15th October 2004 new KDE e.V. statutes are actice. You can find
them (in German only at the moment) on the "Statutes and Articles" page.

