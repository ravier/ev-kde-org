---
title: "Agenda General Assembly KDE e.V. 2021"
layout: page
---

This is the **final** agenda for the Annual General Assembly (AGM) of KDE e.V. 2021.
It takes place online on KDE servers on Monday June 21st (2021-06-21) starting at 11:00 UTC.


## Agenda

1. Welcome
2. Election of a chairman for the general assembly
3. Report of the board (confidential part only)
   1. Report about activities
   2. Report of the treasurer
   3. Report of the auditors of accounting
   4. Relief of the board
4. Report of representatives, working groups and task forces of KDE e.V. (confidential part only)
   1. Report of the representatives to the KDE Free Qt Foundation
   2. Report of the Advisory Board Working Group
   3. Report of the Community Working Group
   4. Report of the Financial Working Group
   5. Report of the KDE Free Qt Working Group
   6. Report of the Fundraising Working Group
   7. Report of the Onboarding Working Group
   8. Report of the System Administration Working Group
6. Election of Auditors of Accounting
7. Election of Representatives to the KDE Free Qt Foundation
8. Votes introduced by members
   1. Hiring a managing director
   2. FLA 2.0
9. Miscellaneous


### Notes

 - There are **no** board elections in this year.


### Edit history

 - 2021-06-05 Vote *Hiring a managing director* added. The text of the
   voting proposal was made available on the appropriate channels.
 - 2021-06-07 Vote *FLA 2.0* added. The proposal was sent to the board
   by a member of KDE e.V.
